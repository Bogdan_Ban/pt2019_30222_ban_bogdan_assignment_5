package ro.tuc.pt._assign5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Run {
	private List<MonitoredData> data;
	public Run() {
		readData();
		countDays();
		activityAppeared();
		activityAppearedDay();
		eachLine();
		activityAppearedTotal();
	}
	private void readData() {
		SimpleDateFormat date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Stream<String> stream;
		try {
			stream = Files.lines(Paths.get("Activities.txt"));
			{
				data=stream
					.map(line->line.split("		"))
					.map(line->{
						try {
							return new MonitoredData(date.parse(line[0]),date.parse(line[1]),line[2]);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						return null;
					})
					.collect(Collectors.toList());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void countDays() {
		System.out.println(
		data.stream()
					.map(d->d.getEndTime().getTime()-d.getStartTime().getTime())
					.reduce(0l,Long::sum) / (1000*60*60*24)
		);
	}
	private void activityAppeared() {
		System.out.println(
		data.stream()
			.collect(Collectors.groupingBy(a->a.getActivityLabel(),Collectors.counting()))
	);
	}
	private void activityAppearedDay() {
		System.out.println(
		data.stream()
			.collect(Collectors.groupingBy(a->a.getStartTime().getTime()/(1000*60*60*24),Collectors.groupingBy(b->b.getActivityLabel(),Collectors.counting())))
	);
	}
	private void eachLine() {
		System.out.println(
			data.stream()
				.map(d->(d.getEndTime().getTime()-d.getStartTime().getTime())/1000)
				.collect(Collectors.toList())
		);
	}
	private void activityAppearedTotal() {
		System.out.println(data.stream()
				.collect(Collectors.toMap(MonitoredData::getActivityLabel,d->(d.getEndTime().getTime()-d.getStartTime().getTime())/1000,Long::sum))
		);
	}
	
	
}

